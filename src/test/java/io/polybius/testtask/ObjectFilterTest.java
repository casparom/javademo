package io.polybius.testtask;

import com.google.gson.JsonObject;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.*;

import static io.polybius.testtask.Filter.Expression.*;
import static org.junit.Assert.assertEquals;

public class ObjectFilterTest {

    private final ObjectFilter objectFilter = new ObjectFilter();
    private final Map<String, JsonObject> objects = new HashMap();

    private static final String nameField = "name";
    private static final String ageField = "age";
    private static final String cityField = "city";
    private static final String heightField = "height";
    private static final String weightField = "weight";

    public ObjectFilterTest() {
        JsonObject bob = new JsonObject();
        bob.addProperty(nameField, "bob");
        bob.addProperty(ageField, new BigDecimal(12));
        bob.addProperty(cityField, "tallinn");
        bob.addProperty(heightField, new BigDecimal(1.79));
        objects.put("bob", bob);

        JsonObject tom = new JsonObject();
        tom.addProperty(nameField, "tom");
        tom.addProperty(ageField, new BigDecimal(12));
        tom.addProperty(cityField, "tallinn");
        tom.addProperty(heightField, new BigDecimal(1.78));
        objects.put("tom", tom);

        JsonObject tim = new JsonObject();
        tim.addProperty(nameField, "tim");
        tim.addProperty(ageField, new BigDecimal(15));
        tim.addProperty(cityField, "tallinn");
        tim.addProperty(heightField, new BigDecimal(1.78));
        objects.put("tim", tim);
    }

    @Test
    public void whenNoFilterThenReturnsAllObjects() {
        List<JsonObject> result = objectFilter.filter(objects.values(), null);
        assertEquals(new ArrayList<>(objects.values()), result);
    }

    @Test
    public void whenObjectLacksTheFieldThenItsNotAMatch() {
        SimpleFilter filter = new SimpleFilter("nonexistantfield", EQUALS, "foo");
        List<JsonObject> result = this.objectFilter.filter(objects.values(), filter);
        assertEquals(new ArrayList<>(Collections.emptyList()), result);
    }

    @Test
    public void whenEqualsFilterMatchesOneThenReturnsTheOne() {
        SimpleFilter filter = new SimpleFilter(nameField, EQUALS, "bob");
        List<JsonObject> result = this.objectFilter.filter(objects.values(), filter);
        assertEquals(new ArrayList<>(Arrays.asList(objects.get("bob"))), result);
    }

    @Test
    public void whenEqualsFilterMatchesTwoBecauseOfContainLogicThenReturnsTheTwo() {
        SimpleFilter filter = new SimpleFilter(nameField, EQUALS, "o");
        List<JsonObject> result = this.objectFilter.filter(objects.values(), filter);
        assertEquals(new ArrayList<>(Arrays.asList(objects.get("tom"), objects.get("bob"))), result);
    }

    @Test
    public void whenEqualsFilterMatchesTwoThenReturnsTheTwo() {
        SimpleFilter filter = new SimpleFilter(ageField, EQUALS, new BigDecimal(12));
        List<JsonObject> result = this.objectFilter.filter(objects.values(), filter);
        assertEquals(new ArrayList<>(Arrays.asList(objects.get("tom"), objects.get("bob"))), result);
    }

    @Test
    public void whenComparingTwoFieldsAndFindsOneMatchThenReturnsTheOne() {
        SimpleFilter nameFilter = new SimpleFilter(nameField, EQUALS, "tom");
        SimpleFilter ageFilter = new SimpleFilter(ageField, EQUALS, new BigDecimal(12));
        List<JsonObject> result = objectFilter.filter(objects.values(), new FilterChain(nameFilter).and(ageFilter));
        assertEquals(new ArrayList<>(Arrays.asList(objects.get("tom"))), result);
    }

    @Test
    public void whenComparingFourFieldsAndFindsOneMatchThenReturnsTheOne() {
        SimpleFilter nameFilter = new SimpleFilter(nameField, EQUALS, "bob");
        SimpleFilter ageFilter = new SimpleFilter(ageField, EQUALS, new BigDecimal(12));
        SimpleFilter cityFilter = new SimpleFilter(cityField, EQUALS, "tallinn");
        SimpleFilter heightFilter = new SimpleFilter(heightField, EQUALS, new BigDecimal(1.79));
        List<JsonObject> result = objectFilter.filter(
                objects.values(),
                new FilterChain(nameFilter).and(ageFilter).and(cityFilter).and(heightFilter)
        );
        assertEquals(new ArrayList<>(Arrays.asList(objects.get("bob"))), result);
    }

    @Test
    public void whenComparingTwoFieldsAndFindsTwoMatchesThenReturnsTheTwo() {
        SimpleFilter cityFilter = new SimpleFilter(cityField, EQUALS, "tallinn");
        SimpleFilter heightFilter = new SimpleFilter(heightField, EQUALS, new BigDecimal(1.78));
        List<JsonObject> result = objectFilter.filter(
                objects.values(),
                new FilterChain(cityFilter).and(heightFilter)
        );
        assertEquals(new ArrayList<>(Arrays.asList(objects.get("tom"), objects.get("tim"))), result);
    }

    @Test
    public void whenComparingTwoFieldsWithOrAndFindsOneMatchThenReturnsTheOne() {
        SimpleFilter nameFilter = new SimpleFilter(nameField, EQUALS, "jyri");
        SimpleFilter heightFilter = new SimpleFilter(heightField, EQUALS, new BigDecimal(1.79));
        List<JsonObject> result = objectFilter.filter(
                objects.values(),
                new FilterChain(nameFilter).or(heightFilter)
        );
        assertEquals(new ArrayList<>(Arrays.asList(objects.get("bob"))), result);
    }

    @Test
    public void whenComparingThreeFieldsWithOrAndFindsThreeThenReturnsTheThree() {
        SimpleFilter nameFilter1 = new SimpleFilter(nameField, EQUALS, "bob");
        SimpleFilter nameFilter2 = new SimpleFilter(nameField, EQUALS, "tim");
        SimpleFilter heightFilter = new SimpleFilter(heightField, EQUALS, new BigDecimal(1.78));
        List<JsonObject> result = objectFilter.filter(
                objects.values(),
                new FilterChain(nameFilter1).or(nameFilter2).or(heightFilter)
        );
        assertEquals(new ArrayList<>(Arrays.asList(
                objects.get("tom"),
                objects.get("bob"),
                objects.get("tim")
        )), result);
    }

    @Test
    public void whenGreaterThanFilterMatchesOneThenReturnsTheOne() {
        SimpleFilter filter = new SimpleFilter(ageField, GREATER_THAN, new BigDecimal(14));
        List<JsonObject> result = this.objectFilter.filter(objects.values(), filter);
        assertEquals(new ArrayList<>(Arrays.asList(objects.get("tim"))), result);
    }

    @Test
    public void whenGreaterThanOrEqualFilterMatchesThreeThenReturnsTheThree() {
        SimpleFilter filter = new SimpleFilter(heightField, GREATER_THAN_OR_EQUAL, new BigDecimal(1.78));
        List<JsonObject> result = this.objectFilter.filter(objects.values(), filter);
        assertEquals(new ArrayList<>(Arrays.asList(
                objects.get("tom"),
                objects.get("bob"),
                objects.get("tim")
        )), result);
    }

    @Test
    public void whenLessThanFilterMatchesThenReturnsTheMatches() {
        SimpleFilter filter = new SimpleFilter(heightField, LESS_THAN, new BigDecimal(1.79));
        List<JsonObject> result = this.objectFilter.filter(objects.values(), filter);
        assertEquals(new ArrayList<>(Arrays.asList(
                objects.get("tom"),
                objects.get("tim")
        )), result);
    }

    @Test
    public void whenLessThanOrEqualFilterMatchesThenReturnsTheMatches() {
        SimpleFilter filter = new SimpleFilter(heightField, LESS_THAN_OR_EQUAL, new BigDecimal(1.78));
        List<JsonObject> result = this.objectFilter.filter(objects.values(), filter);
        assertEquals(new ArrayList<>(Arrays.asList(
                objects.get("tom"),
                objects.get("tim")
        )), result);
    }

    @Test
    public void testComplicatedFilter() {
        SimpleFilter heightFilter = new SimpleFilter(heightField, LESS_THAN_OR_EQUAL, new BigDecimal(1.70));
        SimpleFilter agefilter = new SimpleFilter(ageField, EQUALS, new BigDecimal(25));
        SimpleFilter cityFilter = new SimpleFilter(cityField, EQUALS, "york");
        SimpleFilter nameFilter1 = new SimpleFilter(nameField, EQUALS, "m");
        SimpleFilter nameFilter2 = new SimpleFilter(nameField, EQUALS, "i");
        SimpleFilter weightFilter = new SimpleFilter(weightField, LESS_THAN_OR_EQUAL, new BigDecimal(80));

        Filter filter = new FilterChain(heightFilter)
                .and(agefilter)
                .or(cityFilter)
                .or(nameFilter1)
                .and(nameFilter2)
                .and(weightFilter);

        Map<String, JsonObject> objects = createManyObjects();
        List<JsonObject> result = this.objectFilter.filter(objects.values(), filter);

        assertEquals(new ArrayList<>(Arrays.asList(
                objects.get("chad"),
                objects.get("tommy"),
                objects.get("ben")
        )), result);
    }

    @Test
    public void testComplicatedFilter2() {
        SimpleFilter heightFilter = new SimpleFilter(heightField, LESS_THAN, new BigDecimal(1.86));
        SimpleFilter nameField1 = new SimpleFilter(nameField, EQUALS, "chad");
        SimpleFilter nameField2 = new SimpleFilter(nameField, EQUALS, "tom");
        SimpleFilter agefilter1 = new SimpleFilter(ageField, GREATER_THAN, new BigDecimal(25));
        SimpleFilter ageFilter2 = new SimpleFilter(ageField, LESS_THAN, new BigDecimal(20));
        SimpleFilter cityFilter = new SimpleFilter(cityField, EQUALS, "a");
        SimpleFilter weightFilter = new SimpleFilter(weightField, LESS_THAN_OR_EQUAL, new BigDecimal(100));

        Filter filterChain = new FilterChain(heightFilter)
                .and(nameField1)
                .or(nameField2)
                .and(agefilter1)
                .and(weightFilter)
                .or(ageFilter2)
                .and(heightFilter)
                .and(cityFilter);

        Map<String, JsonObject> objects = createManyObjects();
        List<JsonObject> result = this.objectFilter.filter(objects.values(), filterChain);

        assertEquals(new ArrayList<>(Arrays.asList(
                objects.get("tom"),
                objects.get("bob"),
                objects.get("chad"),
                objects.get("tim")
        )), result);
    }

    private Map<String, JsonObject> createManyObjects() {
        Map<String, JsonObject> objects = new HashMap(this.objects);

        JsonObject john = new JsonObject();
        john.addProperty(nameField, "john");
        john.addProperty(ageField, new BigDecimal(20));
        john.addProperty(cityField, "tartu");
        john.addProperty(heightField, new BigDecimal(1.60));
        john.addProperty(weightField, new BigDecimal(80));
        objects.put("john", john);

        JsonObject tommy = new JsonObject();
        tommy.addProperty(nameField, "tommy");
        tommy.addProperty(ageField, new BigDecimal(25));
        tommy.addProperty(cityField, "paide");
        tommy.addProperty(heightField, new BigDecimal(1.70));
        tommy.addProperty(weightField, new BigDecimal(87));
        objects.put("tommy", tommy);

        JsonObject chad = new JsonObject();
        chad.addProperty(nameField, "chad");
        chad.addProperty(ageField, new BigDecimal(35));
        chad.addProperty(cityField, "new york");
        chad.addProperty(heightField, new BigDecimal(1.85));
        chad.addProperty(weightField, new BigDecimal(67));
        objects.put("chad", chad);

        JsonObject ben = new JsonObject();
        ben.addProperty(nameField, "ben");
        ben.addProperty(ageField, new BigDecimal(50));
        ben.addProperty(cityField, "york");
        ben.addProperty(heightField, new BigDecimal(1.77));
        ben.addProperty(weightField, new BigDecimal(90));
        objects.put("ben", ben);

        return objects;
    }

}
