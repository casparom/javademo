package io.polybius.testtask;

import com.google.gson.JsonObject;
import org.junit.Test;

import static org.junit.Assert.*;

public class QueryParserTest {

    private QueryParser parser = new QueryParser();

    @Test
    public void returnsNullIfQUeryIsEmptyString() {
        String query = "";

        Filter filter = parser.parse(query);

        assertNull(filter);
    }

    @Test
    public void testSimpleEqualsQuery() {
        String query = "name=bob";

        Filter filter = parser.parse(query);

        JsonObject match = new JsonObject();
        match.addProperty("name", "bobo");
        assertTrue(filter.matches(match));

        JsonObject nonMatch = new JsonObject();
        nonMatch.addProperty("name", "boob");
        assertFalse(filter.matches(nonMatch));
    }

    @Test
    public void testSimpleGreaterThanQuery() {
        String query = "age>15";

        Filter filter = parser.parse(query);

        JsonObject match = new JsonObject();
        match.addProperty("age", 16);
        assertTrue(filter.matches(match));

        JsonObject nonMatch = new JsonObject();
        nonMatch.addProperty("age", 15);
        assertFalse(filter.matches(nonMatch));
    }

    @Test
    public void testSimpleGreaterThanOrEqualQuery() {
        String query = "age>=15";

        Filter filter = parser.parse(query);

        JsonObject match = new JsonObject();
        match.addProperty("age", 16);
        assertTrue(filter.matches(match));

        JsonObject nonMatch = new JsonObject();
        nonMatch.addProperty("age", 14);
        assertFalse(filter.matches(nonMatch));
    }

    @Test
    public void testSimpleLessThanQuery() {
        String query = "age<15";

        Filter filter = parser.parse(query);

        JsonObject match = new JsonObject();
        match.addProperty("age", 14);
        assertTrue(filter.matches(match));

        JsonObject nonMatch = new JsonObject();
        nonMatch.addProperty("age", 15);
        assertFalse(filter.matches(nonMatch));
    }

    @Test
    public void testSimpleLessThanOrEqualQuery() {
        String query = "age<=15";

        Filter filter = parser.parse(query);

        JsonObject match = new JsonObject();
        match.addProperty("age", 14);
        assertTrue(filter.matches(match));

        JsonObject nonMatch = new JsonObject();
        nonMatch.addProperty("age", 16);
        assertFalse(filter.matches(nonMatch));
    }

    @Test
    public void testEqualsNameAndEqualsCityQuery() {
        String query = "name=bob&&city=tallinn";

        Filter filter = parser.parse(query);

        JsonObject match = new JsonObject();
        match.addProperty("name", "bob");
        match.addProperty("city", "tallinn");
        assertTrue(filter.matches(match));

        JsonObject nonMatch = new JsonObject();
        nonMatch.addProperty("name", "bob");
        nonMatch.addProperty("city", "tartu");
        assertFalse(filter.matches(nonMatch));
    }

    @Test
    public void testEqualsNameOrEqualsCityQuery() {
        String query = "name=bob||city=tallinn";

        Filter filter = parser.parse(query);

        JsonObject match1 = new JsonObject();
        match1.addProperty("name", "bob");
        match1.addProperty("city", "tartu");
        assertTrue(filter.matches(match1));

        JsonObject match2 = new JsonObject();
        match2.addProperty("name", "tom");
        match2.addProperty("city", "tallinn");
        assertTrue(filter.matches(match2));

        JsonObject nonMatch = new JsonObject();
        nonMatch.addProperty("name", "tom");
        nonMatch.addProperty("city", "tartu");
        assertFalse(filter.matches(nonMatch));
    }

    @Test
    public void testComplexQuery() {
        String query = "name=bob||city=tallinn&&age>=15||age<10";

        Filter filter = parser.parse(query);

        JsonObject match1 = new JsonObject();
        match1.addProperty("name", "bob");
        match1.addProperty("city", "tartu");
        match1.addProperty("age", 9);
        assertTrue(filter.matches(match1));

        JsonObject match2 = new JsonObject();
        match2.addProperty("name", "tom");
        match2.addProperty("city", "tallinn");
        match2.addProperty("age", 15);
        assertTrue(filter.matches(match2));

        JsonObject nonMatch = new JsonObject();
        nonMatch.addProperty("name", "tom");
        nonMatch.addProperty("city", "tartu");
        nonMatch.addProperty("age", 10);
        assertFalse(filter.matches(nonMatch));
    }
}
