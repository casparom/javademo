package io.polybius.testtask;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class MainTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private final PrintStream originalErr = System.err;

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    @After
    public void restoreStreams() {
        System.setOut(originalOut);
        System.setErr(originalErr);
    }

    @Test
    public void simpleQueryThatMatches() {
        String query = "age>=25";
        String json = "[{\"name\":\"Bobby\",\"age\":25},{\"name\":\"Rob\",\"age\":35}]";

        runProgram(query, json);

        assertEquals(json.trim(), outContent.toString().trim());
        assertEquals("", errContent.toString());
    }

    @Test
    public void simpleQueryThatDoesNotMatch() {
        String query = "name=john";
        String json = "[{\"name\":\"Bobby\",\"age\":25},{\"name\":\"Rob\",\"age\":35}]";

        runProgram(query, json);

        assertEquals("[]", outContent.toString().trim());
        assertEquals("", errContent.toString());
    }

    @Test
    public void complexQueryThatMatchesSome() {
        String query = "name=bob&&age<30||city=Tallinn||age>34&&age<36";
        String json = "[" +
                "{\"name\":\"Bobby\",\"age\":25}," +
                "{\"name\":\"Rob\",\"age\":35}," +
                "{\"name\":\"John\",\"age\":11,\"city\":\"Tallinn\"}," +
                "{\"name\":\"Bob\",\"age\":30,\"city\":\"Tartu\",\"weight\":80}" +
                "]";

        runProgram(query, json);

        assertEquals("[" +
                "{\"name\":\"Bobby\",\"age\":25}," +
                "{\"name\":\"Rob\",\"age\":35}," +
                "{\"name\":\"John\",\"age\":11,\"city\":\"Tallinn\"}" +
                "]", outContent.toString().trim());
        assertEquals("", errContent.toString());
    }

    private void runProgram(String query, String json) {
        try {
            Main.main(new String[] {query, json});
        } catch (Exception e) {
            fail();
        }
    }
}
