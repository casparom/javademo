package io.polybius.testtask;

import com.google.gson.JsonObject;

public interface Filter {

    enum Expression {
        EQUALS,
        GREATER_THAN,
        GREATER_THAN_OR_EQUAL,
        LESS_THAN,
        LESS_THAN_OR_EQUAL
    }

    boolean matches(JsonObject object);
    Filter and(Filter filter);
    Filter or(Filter filter);
}
