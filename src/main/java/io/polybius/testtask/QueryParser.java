package io.polybius.testtask;

import io.polybius.testtask.Filter.Expression;

import java.math.BigDecimal;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class QueryParser {

    private static final String LOGICAL_OPERATOR_REGEX = "(&&|\\|\\|)";
    private static final String EXPRESSION_REGEX = "<=|>=|=|<|>";
    private static final String AND_OPERATOR = "&&";
    private static final String OR_OPERATOR = "||";

    Filter parse(String query) {
        if (query.isEmpty()) {
            return null;
        }
        Pattern pattern = Pattern.compile(LOGICAL_OPERATOR_REGEX);
        Matcher matcher = pattern.matcher(query);
        String[] parts = query.split(LOGICAL_OPERATOR_REGEX);

        Filter filter = convertSimpleQueryToFilter(parts[0]);
        for (int i = 1; i < parts.length; i++) {
            matcher.find();
            Filter newFilter = convertSimpleQueryToFilter(parts[i]);
            if (matcher.group().equals(AND_OPERATOR)) {
                filter = filter.and(newFilter);
            }
            else if (matcher.group().equals(OR_OPERATOR)) {
                filter = filter.or(newFilter);
            }
        }
        return filter;
    }

    private Filter convertSimpleQueryToFilter(String query) {
        String symbol = getExpressionSymbol(query);
        if (symbol == null) {
            throw new RuntimeException("Comparison expression missing");
        }
        String fieldName = query.substring(0, query.indexOf(symbol));
        String fieldValue = query.substring(query.indexOf(symbol)+symbol.length());
        return new SimpleFilter(fieldName, parseExpression(symbol), getConvertedValue(fieldValue));
    }

    private String getExpressionSymbol(String query) {
        Pattern pattern = Pattern.compile(EXPRESSION_REGEX);
        Matcher matcher = pattern.matcher(query);
        if (matcher.find()) {
            return matcher.group(0);
        }
        return null;
    }

    private Expression parseExpression(String symbol) {
        switch (symbol) {
            case "=":
                return Expression.EQUALS;
            case ">":
                return Expression.GREATER_THAN;
            case ">=":
                return Expression.GREATER_THAN_OR_EQUAL;
            case "<":
                return Expression.LESS_THAN;
            case "<=":
                return Expression.LESS_THAN_OR_EQUAL;
            default:
                throw new RuntimeException("Unsupported comparison expression");
        }
    }

    private Object getConvertedValue(String value) {
        if (isNumber(value)) {
            return new BigDecimal(value);
        }
        // Default to string for now since only BigDecimal and String types are supported atm
        return value;
    }


    private boolean isNumber(String value) {
        try {
            new BigDecimal(value);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

}
