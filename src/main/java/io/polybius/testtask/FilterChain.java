package io.polybius.testtask;

import com.google.gson.JsonObject;

import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FilterChain implements Filter {
    enum Operator {
        AND,
        OR,
        NONE
    }

    private List<SimpleEntry<Operator, Filter>> filters = new ArrayList<>();

    public FilterChain(SimpleFilter filter) {
        filters.add(new SimpleEntry(Operator.NONE, filter));
    }

    @Override
    public Filter and(Filter filter) {
        filters.add(new SimpleEntry(Operator.AND, filter));
        return this;
    }

    @Override
    public Filter or(Filter filter) {
        filters.add(new SimpleEntry(Operator.OR, filter));
        return this;
    }

    // TODO: Optimize. The algorithm could be optimized to return earlier if one of the OR clauses matches.
    @Override
    public boolean matches(JsonObject object) {
        List<Evaluation> evaluations = evaluateIndividualExpressions(object);
        return applyLogicOperations(evaluations);
    }

    private List<Evaluation> evaluateIndividualExpressions(JsonObject object) {
        List<Evaluation> evaluations = new ArrayList<>();
        for (int i = 0; i < filters.size(); i++) {
            SimpleEntry<Operator, Filter> SimpleEntry = filters.get(i);
            evaluations.add(new Evaluation(SimpleEntry.getKey(), SimpleEntry.getValue().matches(object)));
        }
        return evaluations;
    }

    private boolean applyLogicOperations(List<Evaluation> evaluations) {
        List<Evaluation> modifiedEvaluations = new ArrayList<>(Collections.singletonList(evaluations.get(0)));
        for (int i = 1; i < evaluations.size(); i++) {
            Evaluation currentEval = evaluations.get(i);
            if (currentEval.logicalOperator == Operator.AND) {
                Evaluation lastInModified = modifiedEvaluations.get(modifiedEvaluations.size()-1);
                modifiedEvaluations.remove(lastInModified);
                modifiedEvaluations.add(new Evaluation(lastInModified.logicalOperator, currentEval.value && lastInModified.value));
            }
            else {
                modifiedEvaluations.add(currentEval);
            }
        }
        for (int i = 0; i < modifiedEvaluations.size(); i++) {
            if (modifiedEvaluations.get(i).value) {
                return true;
            }
        }
        return false;
    }

    private class Evaluation {
        Operator logicalOperator;
        boolean value;

        Evaluation(Operator logicalOperator, boolean value) {
            this.logicalOperator = logicalOperator;
            this.value = value;
        }
    }
}
