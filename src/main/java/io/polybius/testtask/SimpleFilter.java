package io.polybius.testtask;

import com.google.gson.JsonObject;

import java.math.BigDecimal;

public class SimpleFilter implements Filter {

    private class UnsupportedDataTypeException extends RuntimeException {
        public UnsupportedDataTypeException() {
            super("Unsupported data type");
        }
    }

    private String field;
    private Expression expression;
    private Object value;

    public SimpleFilter(String field, Expression expression, Object value) {
        this.field = field;
        this.expression = expression;
        this.value = value;
    }

    @Override
    public boolean matches(JsonObject object) {
        if (!object.has(field)) {
            return false;
        }
        Object fieldValue = getFieldValueInCorrectType(object);
        switch (expression) {
            case EQUALS:
                return checkEquals(fieldValue);
            case GREATER_THAN:
                return checkGreaterThan(fieldValue);
            case GREATER_THAN_OR_EQUAL:
                return checkGreaterThanOrEqual(fieldValue);
            case LESS_THAN:
                return checkLessThan(fieldValue);
            case LESS_THAN_OR_EQUAL:
                return checkLessThanOrEqual(fieldValue);
            default:
                return false;
        }
    }

    @Override
    public Filter and(Filter filter) {
        return new FilterChain(this).and(filter);
    }

    @Override
    public Filter or(Filter filter) {
        return new FilterChain(this).or(filter);
    }

    private Object getFieldValueInCorrectType(JsonObject object) {
        if (value instanceof String) {
            return object.get(field).getAsString();
        }
        else if (value instanceof BigDecimal) {
            return object.get(field).getAsBigDecimal();
        }
        throw new UnsupportedDataTypeException();
    }

    private boolean checkEquals(Object fieldValue) {
        if (value instanceof String) {
            return contains((String)fieldValue);
        }
        if (value instanceof BigDecimal) {
            return ((BigDecimal) value).compareTo((BigDecimal) fieldValue) == 0;
        }
        throw new UnsupportedDataTypeException();
    }

    private boolean checkGreaterThan(Object fieldValue) {
        if (value instanceof BigDecimal) {
            return ((BigDecimal) fieldValue).compareTo((BigDecimal) value) > 0;
        }
        throw new UnsupportedDataTypeException();
    }

    private boolean checkGreaterThanOrEqual(Object fieldValue) {
        if (value instanceof BigDecimal) {
            return checkEquals(fieldValue) || ((BigDecimal) fieldValue).compareTo((BigDecimal) value) > 0;
        }
        throw new UnsupportedDataTypeException();
    }

    private boolean checkLessThan(Object fieldValue) {
        if (value instanceof BigDecimal) {
            return ((BigDecimal) fieldValue).compareTo((BigDecimal) value) < 0;
        }
        throw new UnsupportedDataTypeException();
    }

    private boolean checkLessThanOrEqual(Object fieldValue) {
        if (value instanceof BigDecimal) {
            return checkEquals(fieldValue) || ((BigDecimal) fieldValue).compareTo((BigDecimal) value) < 0;
        }
        throw new UnsupportedDataTypeException();
    }

    //decided to make it case insensitive
    private boolean contains(String fieldValue) {
        return fieldValue.toLowerCase().contains(((String) this.value).toLowerCase());
    }

}
