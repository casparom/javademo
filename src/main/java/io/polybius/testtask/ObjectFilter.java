package io.polybius.testtask;

import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ObjectFilter {
    public List<JsonObject> filter(Collection<JsonObject> objects, Filter filter) {
        if (filter == null) {
            return new ArrayList<>(objects);
        }
        List<JsonObject> result = new ArrayList<>();
        objects.forEach((o) -> {
            if (filter.matches(o)) {
                result.add(o);
            }
        });
        return result;
    }
}
